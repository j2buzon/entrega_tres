class CreditLine
  include Dynamoid::Document
  belongs_to :admin
  has_many :payment_plans
  
   index [ :admin_ids]

  field :name
  field :interest_rate, :float
  attr_accessible :interest_rate, :name
end
