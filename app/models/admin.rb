class Admin 
  include Dynamoid::Document

  table :name => :admins
  has_many :credit_lines
  has_many :payment_plans


  before_save :encrypt_password
  after_save :clear_password

  field :email
  field :encrypted_password
  field :salt
  field :pymeName
  field :name

  index [ :email, :encrypted_password]

  EMAIL_REGEX = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i

  validates_presence_of :email, :password, :password_confirmation
  validates_format_of :email, :with => EMAIL_REGEX
  #validates_uniqueness_of :email
  validates_confirmation_of :password
  validates_length_of :password, :in => 6..20, :on => :create

  attr_accessor :password, :password_confirmation

  def self.authenticate(login_email="", login_password="")

    if  EMAIL_REGEX.match(login_email)    
      #user = Admin.find(:email)
      user = Admin.where(:email => login_email).all
    end

    if user && user[0] && user[0].match_password(login_password)
      return user
    else
      return false
    end
  end   

  def match_password(login_password="")
    encrypted_password == BCrypt::Engine.hash_secret(login_password, salt)
  end

  def encrypt_password
    unless password.blank?
      self.salt = BCrypt::Engine.generate_salt
      self.encrypted_password = BCrypt::Engine.hash_secret(password, salt)
    end
  end

  def clear_password
    self.password = nil
  end

end
