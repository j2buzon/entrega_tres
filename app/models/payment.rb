class Payment
  include Dynamoid::Document
  belongs_to :payment_plan
  attr_accessible :paymentAmortization, :paymentInterest, :paymentNumber, :pendingBalance, :totalPayment

  field :paymentAmortization, :float
  field :paymentInterest,:float
  field :paymentNumber,:integer
  field :pendingBalance, :float
  field :totalPayment, :float

end
