class PaymentPlan
  include Dynamoid::Document
  has_many :payments
  belongs_to :admin
  belongs_to :creditLine

  field :birthdate, :datetime
  field :identification
  field :numberOfPayments, :integer
  field :principal, :float
  field :riskLevel, :integer
  field :state
  attr_accessible :birthdate, :identification, :numberOfPayments, :principal, :riskLevel, :state
end
