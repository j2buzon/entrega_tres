class LoginsController < ApplicationController

	before_filter :is_authenticated_user, :except => [:login, :authenticate]
  	
  	def authenticate
      puts "entro a autenticate"
  		if (params[:email] == "" || params[:password] == "")
  			flash[:notice] = "Required Username and Password"
        	flash[:color]= "invalid"
			render "login"
		else
			authorized_user = Admin.authenticate(params[:email], params[:password])
			if authorized_user
				session[:user_id] = authorized_user[0].id
				flash[:notice] = "Wow Welcome again, you logged in as #{authorized_user[0].email}"
				redirect_to(credit_lines_path)
			else
				flash[:notice] = "Invalid Username or Password"
	        	flash[:color]= "invalid"
				render "login"	
			end
	    end
  	end

  	def home
      @adm = Admin.find(session[:user_id])
      puts(@adm.pymeName)
      puts "home"
  	end

	def logout
		session[:user_id] = nil
		redirect_to :action => 'login'
	end

end