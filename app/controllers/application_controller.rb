class ApplicationController < ActionController::Base
  protect_from_forgery
  protected
  def is_authenticated_user
    unless session[:user_id]
      redirect_to(:controller => 'logins', :action => 'login')
      return false
    else
      # set current_user by the current user object
      @current_user = session[:user_id]
      return true
    end
  end
end
