class CreditLinesController < ApplicationController

  before_filter :is_authenticated_user, :except => [:login, :authenticate]

  # GET /credit_lines
  # GET /credit_lines.json
  def index
    @adm = Admin.find(session[:user_id])
    puts "  qqqqqq" + @adm.id
    @array=[]
    @credit_lines = CreditLine.where(:admin_ids => @adm.id).all#all#@adm.credit_lines
    if @credit_lines.nil?
      @credit_lines.each do |credit_line|
        if credit_line.admin.id == session[:user_id]
        @array.push(credit_line)
        end
      end
    end

    puts(@credit_lines)
    puts(CreditLine.all)
    @array1 = PaymentPlan.all
    @payment_plans =[]
    @array1.each do |payment_plan|
      if payment_plan.admin.id == session[:user_id]
        @payment_plans.push(payment_plan)
      end
    end

    puts (@payment_plans.size)
    respond_to do |format|
      format.html # index.html.erb
      #format.json { render json: @credit_lines }
    end
  end

  # GET /credit_lines/1
  # GET /credit_lines/1.json
  def show
    @credit_line = CreditLine.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @credit_line }
    end
  end

  # GET /credit_lines/new
  # GET /credit_lines/new.json
  def new
    @credit_line = CreditLine.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @credit_line }
    end
  end

  # GET /credit_lines/1/edit
  def edit
    @credit_line = CreditLine.find(params[:id])
  end

  # POST /credit_lines
  # POST /credit_lines.json
  def create
    @adm = Admin.find(session[:user_id])
    @credit_line = @adm.credit_lines.create(params[:credit_line])#CreditLine.new(params[:credit_line])
    respond_to do |format|
      if @credit_line
        format.html { redirect_to @credit_line, notice: 'Credit line was successfully created.' }
        format.json { render json: @credit_line, status: :created, location: @credit_line }
      else
        format.html { render action: "new" }
        format.json { render json: @credit_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /credit_lines/1
  # PUT /credit_lines/1.json
  def update
    @credit_line = CreditLine.find(params[:id])

    respond_to do |format|
      if @credit_line.update_attributes(params[:credit_line])
        format.html { redirect_to @credit_line, notice: 'Credit line was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @credit_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /credit_lines/1
  # DELETE /credit_lines/1.json
  def destroy
    @credit_line = CreditLine.find(params[:id])
    @credit_line.destroy

    respond_to do |format|
      format.html { redirect_to credit_lines_url }
      format.json { head :no_content }
    end
  end
end
