class PaymentsController < ApplicationController
  # GET /payments
  # GET /payments.json
  def index
    tiempoInicial=Time.now
    t=PaymentPlan.find_by_state('In progress')

    t=PaymentPlan.where(:state=>'In progress').all
    puts(t.size)
    puts(PaymentPlan.where(:state=>'In progress'))

    if(t.nil?)
      puts('no enraaa')
    else
      credit=CreditLine.find(t.creditLine.id)
      puts(credit.name)
      cuotaAmortizacion=(t.principal/t.numberOfPayments)
      capitalVivo=t.principal #-AMORTIZACION
      interes=capitalVivo*(credit.interest_rate/100)
      #payment=t.payments.create(paymentNumber: 0, paymentInterest: 0, paymentAmortization: 0, pendingBalance: capitalVivo, totalPayment:0)
      payment.save
      cont=0
      totalPagado=0
      capitalVivo=t.principal
      puts(t.numberOfPayments)
      while cont.to_i < t.numberOfPayments
        cont= cont.to_i+1
        puts(cont)

        capitalVivo=capitalVivo-cuotaAmortizacion
        interes=capitalVivo*(credit.interest_rate/100)
        amort=cuotaAmortizacion + interes.abs
        totalPagado=totalPagado+(amort.abs)
        #payment=t.payments.create(paymentNumber: cont, paymentInterest: interes, paymentAmortization: cuotaAmortizacion, pendingBalance: capitalVivo, totalPayment:totalPagado)
        payment.save
      end
      puts"guardooo"
      risk=0
      while (Time.now.sec - tiempoInicial.sec<25)
        risk=rand(10)
      end
      puts(risk)
      t.update_attribute(:riskLevel,risk)

      t.update_attribute(:state, 'Completed')
      t.save
    end

    #------------
    @payments = Payment.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @payments }
    end
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
    @payment = Payment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @payment }
    end
  end

  # GET /payments/new
  # GET /payments/new.json
  def new
    @payment = Payment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @payment }
    end
  end

  # GET /payments/1/edit
  def edit
    @payment = Payment.find(params[:id])
  end

  # POST /payments
  # POST /payments.json
  def create
    @payment = Payment.new(params[:payment])

    respond_to do |format|
      if @payment.save
        format.html { redirect_to @payment, notice: 'Payment was successfully created.' }
        format.json { render json: @payment, status: :created, location: @payment }
      else
        format.html { render action: "new" }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /payments/1
  # PUT /payments/1.json
  def update
    @payment = Payment.find(params[:id])

    respond_to do |format|
      if @payment.update_attributes(params[:payment])
        format.html { redirect_to @payment, notice: 'Payment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment = Payment.find(params[:id])
    @payment.destroy

    respond_to do |format|
      format.html { redirect_to payments_url }
      format.json { head :no_content }
    end
  end
end
