class AdminsController < ApplicationController
  def new
  	@admin = Admin.new
  end

  def create
		@admin = Admin.new(params[:admin])
		if @admin.save
			flash[:notice] = "You Signed up successfully"
	    	flash[:color]= "valid"
		else
	   		flash[:notice] = "Form is invalid"
	   		flash[:color]= "invalid"
    end
    redirect_to credit_lines_path
		#render "new"
	end
end
