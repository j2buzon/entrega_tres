class PaymentPlansController < ApplicationController
  # GET /payment_plans
  # GET /payment_plans.json
  def index
    @payment_plans = PaymentPlan.all

    @payment_plans.each do |payment_plan|
      puts(payment_plan.creditLine)
      puts(payment_plan.creditLine.id)
      puts(CreditLine.find(payment_plan.creditLine.id))
      puts(CreditLine.find(payment_plan.creditLine.id).name)

    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @payment_plans }
    end
  end

  # GET /payment_plans/1
  # GET /payment_plans/1.json
  def show
    @payment_plan = PaymentPlan.find(params[:id])
    @payments= @payment_plan.payments.sort_by { |e|e[:numberOfPayments]}

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @payment_plan }
    end
  end

  # GET /payment_plans/new
  # GET /payment_plans/new.json
  def new
    @payment_plan = PaymentPlan.new

    @credit_lines=CreditLine.all
    puts(@creditLines)
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @payment_plan }
    end
  end

  # GET /payment_plans/1/edit
  def edit
    @payment_plan = PaymentPlan.find(params[:id])
  end

  # POST /payment_plans
  # POST /payment_plans.json
  def create
    puts('entro a crear')
    puts(params[:payment_plan][:creditLine])
    @creditLines= CreditLine.all()
    puts(params[:payment_plan][:birthDate])
    @credit=CreditLine.find(params[:payment_plan][:creditLine])

    par=params[:payment_plan]
    puts("ver la fecha")
    puts(par)
    puts(par["birthdate(1i)"].to_i)
    #date = DateTime.new(par["birthdate(1i)"].to_i, par["birthDate(2i)"].to_i, par["birthDate(3i)"].to_i)
    date=Time.zone.now
    @adm = Admin.find(session[:user_id])

    $i = 1
    $num = 50
    @array=[]
    while $i < $num  do
      @payment_plan = @adm.payment_plans.create(identification: par[:identification], numberOfPayments: par[:numberOfPayments], principal: par[:principal], riskLevel: par[:riskLevel], state: par[:state],creditLine:@credit, state: 'In progress')
      @array.push(@payment_plan)
      $i +=1
    end
    #@payment_plan = @adm.payment_plans.create(identification: par[:identification], numberOfPayments: par[:numberOfPayments], principal: par[:principal], riskLevel: par[:riskLevel], state: par[:state],creditLine:@credit, state: 'In progress')

   # @payment_plan.identification = par[:identification]
    puts(@credit.payment_plans.size)
    puts(PaymentPlan.all.size)
    puts(@payment_plan.id)

    #-------------------
    require 'yaml'
    require 'aws-sdk'

    config_file = File.join(File.dirname(__FILE__),"config.yml")
    config = YAML.load(File.read(config_file))
    AWS.config(config)

    queue_name ='PaymentPlansQueue'
    sqs = AWS::SQS.new
    puts "Creating queue '#{queue_name}' ..."
    q = nil
    begin
      # Creates new queue or gets existing queue
      q = sqs.queues.create queue_name
    rescue AWS::SQS::Errors::InvalidParameterValue => e
      puts "Invalid queue name '#{queue_name}'. "+e.message
      exit 1
    end
    puts "Waiting for queue created on AWS"
    loop do
      if q.exists?
        puts "Queue is created on AWS"
        break
      end
      sleep 1
    end
    @array.each do |plan|
       m = q.send_message plan.id
    end
    #-----------
    #@payment_plan.creditLine = @credit
    #@payment_plan.state = 'In progress'
    #puts(@credit.id)
    #puts(@payment_plan.creditLine.id)
    #puts(CreditLine.find(@payment_plan.creditLine.id).name)
    #puts('chap')

   # @payment_plan = PaymentPlan.new(params[:payment_plan])

    respond_to do |format|
      #if @payment_plan.save
        format.html { redirect_to @payment_plan, notice: 'Payment plan was successfully created.' }
        format.json { render json: @payment_plan, status: :created, location: @payment_plan }
      #else
        format.html { render action: "new" }
        format.json { render json: @payment_plan.errors, status: :unprocessable_entity }
      #end
    end
  end

  # PUT /payment_plans/1
  # PUT /payment_plans/1.json
  def update
    @payment_plan = PaymentPlan.find(params[:id])

    respond_to do |format|
      if @payment_plan.update_attributes(params[:payment_plan])
        format.html { redirect_to @payment_plan, notice: 'Payment plan was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @payment_plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payment_plans/1
  # DELETE /payment_plans/1.json
  def destroy
    @payment_plan = PaymentPlan.find(params[:id])
    @payment_plan.destroy

    respond_to do |format|
      format.html { redirect_to payment_plans_url }
      format.json { head :no_content }
    end
  end
end
