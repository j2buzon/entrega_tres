class CreatePaymentPlans < ActiveRecord::Migration
  def change
    create_table :payment_plans do |t|
      t.datetime :birthdate
      t.string :identification
      t.integer :numberOfPayments
      t.float :principal
      t.integer :riskLevel
      t.string :state
      t.references :creditLine

      t.timestamps
    end
    add_index :payment_plans, :creditLine_id
  end
end
