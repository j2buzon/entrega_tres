class CreateCreditLines < ActiveRecord::Migration
  def change
    create_table :credit_lines do |t|
      t.decimal :interest_rate
      t.string :name

      t.timestamps
    end
  end
end
